public class DatatypeConversion {
    public static void main(String args[]){
        int x;
        Float y=new Float("4.96");
        x=y.intValue();
        System.out.println("int value is:"+x);
        double z=y.doubleValue();
        System.out.println("double value is:"+z);
        long a=y.longValue();
        System.out.println("long value is:"+a);
        short b=y.shortValue();
        System.out.println("short value is:"+b);
        
    }
}
