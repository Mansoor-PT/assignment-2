package assignment2;

public class Main {
    public static void main(String args[]){
        Tostring obj=new Tostring(123,"mansjs","zoho");
        System.out.println(obj);
        
    }
}
class Tostring {
    private int id;
    private String name;
    private String company;

    public Tostring(int id, String name, String company) {
        this.id = id;
        this.name = name;
        this.company = company;
    }
@Override
    public String toString() { 
        return String.format("id: " +id+",name:"+name+",company:"+company); 
    } 
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
    
}
